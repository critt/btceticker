
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.*;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.SSLSocket;
import java.math.BigDecimal;
import java.util.Calendar;
import java.text.SimpleDateFormat;

public class Ticker
{
	//instance variables as place holders preceding formatting
	public double high, low, avg, vol, volCur, last, buy, sell; 
    public Long updated, serverTime;
    private String stringToParse;
    private String[] useful;

	//constructor
	public Ticker()
	{
		high = 0;
		low = 0;
		avg = 0;
		vol = 0;
		volCur = 0;
		last = 0;
		buy = 0;
		sell = 0;
		updated = 0l;
        serverTime = 0l;
        stringToParse = " ";
        useful = new String[20];
	}
	
	//go method calls all important methods (call this in other classes rather than calling them individually)
	public void go() throws FileNotFoundException, IOException
	{
		getJson();
		makeArray();
		setVars();
	}

	//getJson method sets instance variable stringToParse to JSON string fetched from BTC-E API
	public void getJson() throws FileNotFoundException, IOException
	{
		InputStream in = new URL("https://btc-e.com/api/2/btc_usd/ticker/").openStream();
		BufferedReader reader = new BufferedReader(new InputStreamReader(in));
		StringBuilder out = new StringBuilder();
		String line;
		while ((line = reader.readLine()) != null)
		{
			out.append(line);
		}
		reader.close();

		String rawJson = out.toString();
		stringToParse = rawJson;
	}

	//makeArray method sets the elements in instance variable useful[] based on .split method
	//applied to stringToParse
	public void makeArray()
	{
		useful = stringToParse.split("[:,}]");
	}

	//setDoubles method sets double instance variables to corresponding elements from useful[]
	public void setVars()
	{
		high = Double.parseDouble(useful[2]);
		low = Double.parseDouble(useful[4]);
		avg = Double.parseDouble(useful[6]);
		vol = Double.parseDouble(useful[8]);
		volCur = Double.parseDouble(useful[10]);
		last = Double.parseDouble(useful[12]);
		buy = Double.parseDouble(useful[14]);
		sell = Double.parseDouble(useful[16]);
		updated = Long.parseLong(useful[18]);
	    serverTime = Long.parseLong(useful[20]);
	}
	
	//format method takes a double, strips trailing zeros, and returns a string
	public String format(Double j)
	{
		String f;
		f = new BigDecimal(Double.toString(j)).stripTrailingZeros().toPlainString();
		return f;
	}
	
	//dateStringFromUnixLong method takes a unix time long and returns a date and time string
	public String format(Long unixLong)
	{
	    SimpleDateFormat f = new SimpleDateFormat("HH:mm:ss z");
	    Calendar c = Calendar.getInstance();
	    c.setTimeInMillis(unixLong*1000);
	    String dateString = "" + f.format(c.getTime());
	    return dateString;
	}
}
