
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;



public class BTCEConsoleTicker
{
	public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException
	{
		Ticker ticker = new Ticker();
		int i = 1;
		while(i==1)
		{
			ticker.go();
			System.out.println("------BTC-E Ticker------");
			System.out.println("Current buy price: " + ticker.format(ticker.buy));
			System.out.println("Current sell price: " + ticker.format(ticker.sell));
			System.out.println("High: " + ticker.format(ticker.high));
			System.out.println("Low: " + ticker.format(ticker.low));
			System.out.println("Average: " + ticker.format(ticker.avg));
			System.out.println("Volume: " + ticker.format(ticker.vol));
			System.out.println("Current Volume: " + ticker.format(ticker.volCur));
			System.out.println("Last: " + ticker.format(ticker.last));
			System.out.println("Last updated: " + ticker.format(ticker.updated));
			System.out.println("Server time: " + ticker.format(ticker.serverTime));
			Thread.sleep(5000);
		}
	
	}
}