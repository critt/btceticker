import javax.swing.*;

import java.awt.*;
import java.io.FileNotFoundException;
import java.io.IOException;


//Changelog:
//Removed individual ticker method calls and replaced them with go() method call in both lines 20 and 59.
//Updated JButton text fields to reference formatted instance variables in the Ticker class.
//Updated JButton .setText(String) parameters to reference formatted instance variables in the Ticker class.


public class GUI extends JFrame
{
	public static void main(String[] args) throws FileNotFoundException, IOException, InterruptedException
    {
	
	//instantiate new Ticker object and call it's go method
	Ticker ticker = new Ticker();
	ticker.go();
    
	//JFrame declaration and initialization
    JFrame frame = new JFrame("BTC-E Ticker");
    frame.setSize(400,400);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setVisible(true);
    frame.requestFocus();
    
    //JPanel declaration and initialization
    JPanel panel = new JPanel(new GridLayout(5, 2));
    
    //JButton declarations
    JButton bBuy, bSell, bHigh, bLow, bAverage, bVolume, bCVol, bLast, bUpdated, bServerTime;

    //JButton initializations
    bBuy = new JButton("BUY: " + ticker.format(ticker.buy));
    bSell = new JButton("SELL: " + ticker.format(ticker.sell));
    bHigh = new JButton("HIGH: " + ticker.format(ticker.high));
    bLow = new JButton("LOW: " + ticker.format(ticker.low));
    bAverage = new JButton("AVG: " + ticker.format(ticker.avg));
    bVolume = new JButton("VOL: " + ticker.format(ticker.vol));
    bCVol = new JButton("CVOL: " + ticker.format(ticker.volCur));
    bLast = new JButton("LAST: " + ticker.format(ticker.last));
    bUpdated = new JButton("TIME: " + ticker.format(ticker.updated));
    bServerTime = new JButton("STIME: " + ticker.format(ticker.serverTime));
    
    //add buttons to panel and panel to frame
    frame.add(panel);
    panel.add(bBuy);
    panel.add(bSell);
    panel.add(bHigh);
    panel.add(bLow);
    panel.add(bAverage);
    panel.add(bVolume);
    panel.add(bCVol);
    panel.add(bLast);
    panel.add(bUpdated);
    panel.add(bServerTime);

    //loop for ticking functionality
    int i = 1;
    while(i==1)
    {
    	ticker.go();
    	
    	bBuy.setText("BUY: " + ticker.format(ticker.buy));
        bSell.setText("SELL: " + ticker.format(ticker.sell));
        bHigh.setText("HIGH: " + ticker.format(ticker.high));
        bLow.setText("LOW: " + ticker.format(ticker.low));
        bAverage.setText("AVG: " + ticker.format(ticker.avg));
        bVolume.setText("VOL: " + ticker.format(ticker.vol));
        bCVol.setText("CVOL: " + ticker.format(ticker.volCur));
        bLast.setText("LAST: " + ticker.format(ticker.last));
        bUpdated.setText("TIME: " + ticker.format(ticker.updated));
        bServerTime.setText("STIME: " + ticker.format(ticker.serverTime));
    	
    	panel.updateUI();
    	Thread.sleep(5000);
    }
  }
}